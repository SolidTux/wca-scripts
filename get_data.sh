#!/bin/bash

set -ex

rm -Rf data

mkdir data
push data
wget 'https://www.worldcubeassociation.org/export/results/WCA_export.tsv'
unzip WCA_export.tsv
