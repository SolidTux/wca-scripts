#!/usr/bin/env python3

import pandas as pd
import math
import sys

if len(sys.argv) < 2:
    print("WCA ID has to be provided.")
    exit(1)

wca_id = sys.argv[1]

persons = pd.read_csv("data/WCA_export_Persons.tsv", sep="\t")
person = persons[persons["id"] == wca_id]
country = person.iloc[-1]["countryId"]
countries = pd.read_csv("data/WCA_export_Countries.tsv", sep="\t")
continent = countries[countries["name"] == country].iloc[0]["continentId"]
continent_countries = list(
    countries[countries["continentId"] == continent].iloc[:]["name"]
)
num_total = len(persons.index)
num_country = len(persons[persons["countryId"] == country].index)
events = pd.read_csv("data/WCA_export_Events.tsv", sep="\t")

ranks_sgl_tot = pd.read_csv(
    "data/WCA_export_RanksSingle.tsv",
    sep="\t",
    dtype={"personId": object, "eventId": object},
)
ranks_sgl = ranks_sgl_tot[ranks_sgl_tot["personId"] == wca_id]
ranks_avg_tot = pd.read_csv(
    "data/WCA_export_RanksAverage.tsv",
    sep="\t",
    dtype={"personId": object, "eventId": object},
)
ranks_avg = ranks_avg_tot[ranks_avg_tot["personId"] == wca_id]


def hundreths_to_string(time):
    s, h = divmod(time, 100)
    m, s = divmod(s, 60)
    return "%02d:%02d.%02d" % (m, s, h)


def format_rank(r, t):
    return "%6d / %6d (%.2f%%)" % (r, t, 100.0 * float(r) / float(t))


def format_row(avg, row):
    if row["eventId"] == "333fm":
        if avg:
            row["best"] = str(row["best"] / 100.0)
        else:
            row["best"] = str(row["best"])
    elif row["eventId"] == "333mbf":
        wrong = int(str(row["best"])[7:])
        time = int(str(row["best"])[2:7])
        points = 99 - int(str(row["best"])[0:2])
        right = points + wrong
        total = points + 2 * wrong
        m, s = divmod(time, 60)
        row["best"] = "%d / %d %02d:%02d" % (right, total, m, s)
    else:
        row["best"] = hundreths_to_string(row["best"])
    if avg:
        ranks = ranks_avg_tot
    else:
        ranks = ranks_sgl_tot
    eventranks = ranks[ranks["eventId"] == row["eventId"]]
    num_world = len(eventranks.index)
    num_country = len(
        eventranks[
            eventranks["personId"].isin(persons[persons["countryId"] == country]["id"])
        ]
    )
    num_continent = len(
        eventranks[
            eventranks["personId"].isin(
                persons[persons["countryId"].isin(continent_countries)]["id"]
            )
        ]
    )
    row["worldRankRel"] = row["worldRank"] / num_world
    row["worldRank"] = format_rank(row["worldRank"], num_world)
    row["continentRank"] = format_rank(row["continentRank"], num_continent)
    row["countryRank"] = format_rank(row["countryRank"], num_country)
    row["eventNum"] = events[events["id"] == row["eventId"]].index[0]
    row["eventId"] = events[events["id"] == row["eventId"]].iloc[0]["name"]
    if avg:
        row["eventId"] += " (Average)"
    else:
        row["eventId"] += " (Single)"
    return row


ranks_sgl = ranks_sgl.apply(lambda x: format_row(False, x), axis=1)
ranks_avg = ranks_avg.apply(lambda x: format_row(True, x), axis=1)
ranks = (
    pd.concat([ranks_sgl, ranks_avg])
    .drop("personId", axis=1)
    # .sort_values(by=["eventNum", "eventId"])
    .sort_values(by="worldRankRel")
    .reset_index(drop=True)
)
ranks.drop(["eventNum", "worldRankRel"], axis=1, inplace=True)
ranks.columns = [
    "Event",
    "Personal Record",
    "World Rank",
    "Continent Rank",
    "National Rank",
]

nind = int(math.ceil(math.log10(len(ranks) - 1)))
for line in ranks.to_string(justify="center", index=True).splitlines():
    print(line[nind:])
